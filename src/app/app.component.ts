import { AfterViewInit, Component, ElementRef, Renderer2, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements AfterViewInit {

  // @ViewChild('image', { static: false }) image!: ElementRef;
  @ViewChild('carrousell', { static: false }) carrousell!: ElementRef;

  @ViewChild('gallery', { static: false }) gallery!: ElementRef;


  title = 'Upload-Image';
  constructor(private render: Renderer2) { }

  ngAfterViewInit(): void {

  }


  images: Array<string> = new Array<string>;

  public preview: string = '';
  private previewID: number = -1;

  public test(inputFile: any): void {
    const imgFile = inputFile.files[0];

    const reader = new FileReader();
    reader.addEventListener('load', (event: any) => {

      this.images.push(event.target.result);

      // let id = this.images.length;
      // let imgEle = this.render.createElement('img');
      // this.render.setProperty(imgEle, 'src', event.target.result);
      // this.render.setProperty(imgEle, 'id', 'img' + id);
      // this.render.setProperty(imgEle, 'width', 50);
      // this.render.setProperty(imgEle, 'height', 50);

      // this.render.appendChild(this.carrousell.nativeElement, imgEle);
    });

    reader.readAsDataURL(imgFile);
    inputFile.value = "";
    inputFile = undefined;

  }

  public testCanvas() {
    let canvas = this.render.createElement('canvas');
    console.log(canvas);
    debugger;
  }

  public previous() {

    if (this.previewID > 0) {
      this.previewID -= 1;
      console.log(this.images[this.previewID]);
      this.preview = this.images[this.previewID];
    }
  }
  public next() {
    if (this.previewID != undefined && this.images.length > 0 && this.previewID < this.images.length - 1) {
      this.previewID += 1
      this.preview = this.images[this.previewID];
    }
  }

  public view(index: number) {
    console.error("this must be acces by the id and not by the url");
    this.previewID = index;
    this.preview = this.images[index];
  }

  public removeImage(index: number) {
    console.error("this must be the id of the array not the value")
    if (this.previewID == index) {
      this.previewID = -1;
      this.preview = ''; 
    }
    this.images.splice(index, 1);
  }
}
